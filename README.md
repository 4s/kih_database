kih_database
============

[Grails](http://grails.org/)-based national system that collects, stores and makes available telemedical measuring and monitoring data across systems and sectors.

For further documentation see: [http://4s-online.dk](http://4s-online.dk/wiki/doku.php?id=kih-database:overview)
